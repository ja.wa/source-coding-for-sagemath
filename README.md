# Source Coding for SageMath

This repository contains additional source coding methods for Sage/SageMath that are designed for teaching purposes. This means that all schemes provide a verbose output showing the encoding process in detail. The following methods are contained:

* (extension to) *q*-nary Huffman coding
* Shannon coding
* Shannon-Fano coding
* Shannon-Fano-Elias coding
* Arithmetic coding
* Golomb coding
* Elias codings (delta, gamma, omega)
* LZ77 


## Installation Instructions

The easiest way of installation is using one of the offered patches sage-source-coding-6.6.patch and sage-source-coding-6.8.patch for version 6.6 and 6.8, respectively. Using them for other versions of SageMath may fail; then, please use the offered source code and take at look at [https://doc.sagemath.org/](https://doc.sagemath.org/) for further information.

Let `SAGE_ROOT` point to the directory of a compiled SageMath installation. Then, the appropriate patch can be installed using the follwing steps:

1. Download the patch.
2. Change directory to `SAGE_ROOT`.
3. Apply patch using the command `patch -p1 < <path to patch>`.
4. Re-compile SageMath: `./sage -b`.
5. To generate the HTML documentation for the newly added modules, use `./sage --docbuild reference/coding html`.

The installation is complete.


## Authors

* **Jan Wabbersen** - [ja.wa](https://gitlab.com/ja.wa)

## License

See the [license](license.txt) file.