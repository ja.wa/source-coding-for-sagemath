from sage.misc.lazy_import import lazy_import
lazy_import('sage.coding.source_coding.shannon', 'Shannon')
lazy_import('sage.coding.source_coding.shannon_fano', 'ShannonFano')
lazy_import('sage.coding.source_coding.shannon_fano_elias', 'ShannonFanoElias')
lazy_import('sage.coding.source_coding.huffman', 'Huffman')
lazy_import('sage.coding.source_coding.arithmetic_coding', 'ArithmeticCode')
lazy_import('sage.coding.source_coding.golomb', 'Golomb')
lazy_import('sage.coding.source_coding.elias_codes', 'Elias')
lazy_import('sage.coding.source_coding.lz77', 'LZ77')
