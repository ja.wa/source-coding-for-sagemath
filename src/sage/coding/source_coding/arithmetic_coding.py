r"""
Arithmetic Coding

This module implements arithmetic coding with a semi-adaptive model. 

AUTHORS:

- Jan Wabbersen (2015-04-27): initial version.
"""

#*****************************************************************************
#       Copyright (C) 2015 Jan Wabbersen <jan.wabbersen@googlemail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from sage.structure.sage_object import SageObject
from misc import cum_sum, frequency_table, SimpleTable

class ArithmeticCode(SageObject):
    r"""
    This class implements an integer implementation of arithmetic
    coding.
    
    It uses a semi-adaptive model which is built when encoding a string.
    Therefore, an instance of this class can only be used for one
    encoding.
    
    INPUT:

        - ``bits`` -- defines the length of the interval (2^bits).
          
        - ``verbose`` -- (default: False) if True, print intermediate
          data of building the code.
       
    EXAMPLES::
    
        sage: from sage.coding.source_coding.arithmetic_coding import ArithmeticCode
        sage: ac = ArithmeticCode()
        sage: encoded = ac.encode("Encode me!"); encoded
        '11111011100101011011100011010011'
        sage: ac.decode(encoded)
        'Encode me!'

    This example shows how to use the class. If you want to define the
    used interval for encoding and decoding, you can do that with
    ``bits`` and if you want to see intermediate steps of the encoding,
    can use ``verbose``::
    
        sage: ac2 = ArithmeticCode(bits=8, verbose=True)
        sage: ac2.encode("try")
        Cumulative counts and symbol-interval mapping:
        {'y': (0, 1), 'r': (1, 2), 't': (2, 3)}
        | symbol | lower  | upper   | mid rescales | half  | output |
        |        | 0      | 255     | 0            |       |        |
        | t      | 512/3  | 255     | 0            |       |        |
        |        | 256/3  | 255     | 0            | upper | 1      |
        | r      | 1280/9 | 1783/9  | 0            |       |        |
        |        | 256/9  | 1271/9  | 0            | upper | 1      |
        | y      | 256/9  | 1765/27 | 0            |       |        |
        |        | 512/9  | 3557/27 | 0            | lower | 0      |
        | end    |        |         | 0            |       | 01     |
        '11001'
        
    It is an error to try to encode multiple string with one instance::
    
        sage: ac2.encode("Another try.")
        Traceback (most recent call last):
        ...
        ValueError: With an ArithmeticCode instance you can only encode one string.
        
    ALGORITHM:
        
        Based on pseudocode in [Ble13]_.
        
    REFERENCES:
        
        .. [Ble13] G. E. Blelloch. Introduction to data compression.
           http://www.cs.cmu.edu/~guyb/realworld/compression.pdf
    """
    
    def __init__(self, bits=30, verbose=False):
        r"""
        Constructor for ArithmeticCode.

        See the docstring of this class for full documentation.

        EXAMPLES::

            sage: from sage.coding.source_coding.arithmetic_coding import ArithmeticCode
            sage: ac = ArithmeticCode(bits=10)
        """
        # Lower bound of the interval.
        self._lo_bound = 0
        # Upper bound of the interval.
        self._up_bound = 2**bits      
        # Subintervals for rescaling.
        self._first_quarter = self._up_bound / 4
        self._half = self._up_bound / 2
        self._third_quarter = 3 * self._first_quarter
        
        self._verbose = verbose
        self._v_table = SimpleTable(["symbol", "lower", "upper",
                                     "mid rescales", "half", "output"])
        
        # Will be initialized when encoding. 
        self._total = None
        self._symbols = None
        self._intervals = None
        self._dic = None
        
    def _finish_init(self, string):
        r"""
        Build the model for the passed string.

        INPUT:

        - ``string`` -- the string for building the model.
          
        TESTS::
        
            sage: from sage.coding.source_coding.arithmetic_coding import ArithmeticCode
            sage: ac = ArithmeticCode()
            sage: ac._finish_init("Don't do that!")
            sage: ac.encode("This won't work!")
            Traceback (most recent call last):
            ...
            ValueError: With an ArithmeticCode instance you can only encode one string.

        """
        self._total = len(string)
        # Interval arrangement.
        freq_list = frequency_table(string).items()
        self._symbols, frequencies = zip(*freq_list)
        cum_freq = cum_sum(frequencies)
        # For decoding
        self._intervals = zip([0] + cum_freq[:-1], cum_freq)
        # For encoding.
        self._dic = dict(zip(self._symbols, self._intervals))

    def _rescale(self, lo, up, mid_rescales):
        r"""
        Rescale the interval if it is too small.

        INPUT:

        - ``lo`` -- lower bound of the interval.

        - ``up`` -- upper bound of the interval.

        - ``mid_rescales`` -- number of rescales in the middle half,
          since another rescale.
        """
        output = []
        while True:
            out = []
            half = ""  # for verbose
            if lo >= self._half:
                out.append('1')
                up = 2*up - self._up_bound + 1
                lo = 2*lo - self._up_bound
                out.append(mid_rescales * '0')
                mid_rescales = 0
                half = "upper"
            elif up < self._half:
                out.append('0')
                up = 2*up + 1
                lo = 2*lo
                out.append(mid_rescales * '1')
                mid_rescales = 0
                half = "lower"
            elif lo >= self._first_quarter and up < self._third_quarter:
                up = 2*up - self._half + 1
                lo = 2*lo - self._half
                mid_rescales += 1
                half = "middle"
            else:
                break
            output += out
            if self._verbose:
                self._v_table.add_row(["", lo, up, mid_rescales, half, "".join(out)])
        return (lo, up, mid_rescales, output)
        
    def encode(self, string):
        r"""
        Build a model with the passed string and encode the string with
        it.
        
        INPUT:

        - ``string`` -- a string of symbols over an alphabet.
          
        OUTPUT:

        - An arithmetic encoding of ``string``.
        
        EXAMPLES:
        
        This example illustrates how to encode a string::
        
            sage: from sage.coding.source_coding.arithmetic_coding import ArithmeticCode
            sage: ac = ArithmeticCode()
            sage: ac.encode("Encode me!")
            '11111011100101011011100011010011'
        """
        if self._up_bound < 4*len(string):
            raise ValueError("Passed string is too long for the chosen "
                             "interval.")
        if self._total is not None:
            raise ValueError("With an ArithmeticCode instance you can only "
                             "encode one string.")
        self._finish_init(string)
        lo = self._lo_bound
        up = self._up_bound - 1
        output = []
        # Saves number of successive scalings in the middle half.
        mid_rescales = 0
        if self._verbose:
            self._v_table.add_row(["", lo, up, mid_rescales, "", ""])
        for s in string:
            width = up - lo + 1
            s_lo, s_up = self._dic[s]
            up = lo + (width * s_up / self._total) - 1
            lo = lo + (width * s_lo / self._total)
            if self._verbose:
                self._v_table.add_row([s, lo, up, mid_rescales, "", ""])
            lo, up, mid_rescales, out = self._rescale(lo, up, mid_rescales)
            output += out
        # output has to fall in the correct interval.
        if lo >= self._first_quarter:
            out = '10' + mid_rescales*'0'
        else:
            out = '01' + mid_rescales*'1'
        output.append(out)
        if self._verbose:
            print "Cumulative counts and symbol-interval mapping:"
            print self._dic
            self._v_table.add_row(["end", "", "", mid_rescales, "", out])
            self._v_table.print_me()
        return ''.join(output)
    
    def decode(self, string):
        r"""
        Decode the given string based on the current model.
        
        INPUT:

        - ``string`` -- a string of encoded symbols.
          
        OUTPUT:

        - The arithmetic decoding of ``string``.
        
        EXAMPLES:
        
        This example illustrates how to encode and then decode a
        string::
        
            sage: from sage.coding.source_coding.arithmetic_coding import ArithmeticCode
            sage: ac = ArithmeticCode()
            sage: encoded = ac.encode("Encode me, please!")
            sage: ac.decode(encoded)
            'Encode me, please!'
        """
        lo = lo_b = self._lo_bound
        up = up_b = self._up_bound - 1
        j = 1
        pos = 0
        output = []
        while j <= self._total:
            width = up - lo + 1
            i = -1
            while True:
                i += 1
                lower, upper = self._intervals[i]
                up_s = lo + (width * upper / self._total) - 1
                lo_s = lo + (width * lower / self._total)
                in_interval = lo_b >= lo_s and up_b <= up_s
                if i >= len(self._symbols) - 1 or in_interval:
                    break
            if in_interval:
                output.append(self._symbols[i])
                up = up_s
                lo = lo_s
                j += 1
                while True:
                    if lo >= self._half:
                        up = 2*up - self._up_bound + 1
                        lo = 2*lo - self._up_bound
                        up_b = 2*up_b - self._up_bound + 1
                        lo_b = 2*lo_b - self._up_bound
                    elif up < self._half:
                        up = 2*up + 1
                        lo = 2*lo
                        up_b = 2*up_b + 1
                        lo_b = 2*lo_b
                    elif (lo >= self._first_quarter and
                          up < self._third_quarter):
                        up = 2*up - self._half + 1
                        lo = 2*lo - self._half
                        up_b = 2*up_b - self._half + 1
                        lo_b = 2*lo_b - self._half
                    else:
                        break
            else:
                b = string[pos]
                pos += 1
                width_b = up_b - lo_b + 1
                lo_b = lo_b + int(b)*(width_b / 2)
                up_b = lo_b + (width_b / 2) - 1
        return ''.join(output)
