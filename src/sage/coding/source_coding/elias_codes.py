r"""
Elias Codes

This module implements the Elias codes gamma, delta, and omega. 

AUTHORS:

- Jan Wabbersen (2015-04-27): initial version.
"""

#*****************************************************************************
#       Copyright (C) 2015 Jan Wabbersen <jan.wabbersen@googlemail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from math import log

from sage.structure.sage_object import SageObject

class Elias():
    r"""
    This class implements the three Elias codes gamma, delta, and omega.
    
    It can build the Elias codes for positive integers.
    
    INPUT:

        - ``code`` -- one of the strings 'gamma', 'g', 'delta', 'd',
          'omega', 'o'.
       
    EXAMPLES::
    
        sage: from sage.coding.source_coding.elias_codes import Elias
        sage: g = Elias("g")
        sage: g.encode(17)
        '000010001'
        sage: d = Elias("d")
        sage: d.encode(17)
        '001010001'
        sage: o = Elias("o")
        sage: encoded = o.encode(17); encoded
        '10100100010'
        sage: o.decode(encoded)
        17

    This example shows how to use the class.
    """
    def __init__(self, code):
        r"""
        Constructor for Elias.

        See the docstring of this class for full documentation.

        EXAMPLES::

            sage: from sage.coding.source_coding.elias_codes import Elias
            sage: omega = Elias("omega")
        """
        codes = ["g", "gamma", "d", "delta", "o", "omega"]
        if not any(code == c for c in codes):
            raise ValueError("This Elias code does not exist.")
        self._code = code
    
    def encode(self, n):
        r"""
        Encode the given positive integer with the chosen Elias code.
        
        INPUT:

        - ``n`` -- a positive integer.
          
        OUTPUT:

        - An Elias code of ``n``.
        
        EXAMPLES:
        
        This example illustrates how to encode an integer with the
        Elias gamma coding::
        
            sage: from sage.coding.source_coding.elias_codes import Elias
            sage: g = Elias("g")
            sage: g.encode(17)
            '000010001'
        """
        if n < 1:
            raise ValueError("Input must be an integer greater than 0.")
        if self._code == "g" or self._code == "gamma":
            return self._gamma_encode(n)
        elif self._code == "d" or self._code == "delta":
            return self._delta_encode(n)
        elif self._code == "o" or self._code == "omega":
            return self._omega_encode(n)
        
    def decode(self, string):
        r"""
        Decode the given Elias code.
        
        INPUT:

        - ``string`` -- an Elias code.
          
        OUTPUT:

        - The decoding of ``string``.
        
        EXAMPLES:
        
        This example illustrates how to encode and then decode a
        string::
        
            sage: from sage.coding.source_coding.elias_codes import Elias
            sage: o = Elias("o")
            sage: encoded = o.encode(17); encoded
            '10100100010'
            sage: o.decode(encoded)
            17
        """
        if self._code == "g" or self._code == "gamma":
            return self._gamma_decode(string)
        elif self._code == "d" or self._code == "delta":
            return self._delta_decode(string)
        elif self._code == "o" or self._code == "omega":
            return self._omega_decode(string)
    
    def _delta_encode(self, n):
        r"""
        Encode the given positive integer with the Elias delta code.
        
        INPUT:

        - ``n`` -- a positive integer.
          
        OUTPUT:

        - The Elias delta code of ``n``.
        """
        N = int(log(n, 2))
        return self._gamma_encode(N+1) + bin(n)[3:]
    
    def _delta_decode(self, string):
        r"""
        Decode the given Elias delta code.
        
        INPUT:

        - ``string`` -- an Elias delta code.
          
        OUTPUT:

        - The Elias delta decoding of ``string``.
        """
        if string == '1': return 1
        count0 = 0
        while string[count0] == '0':
            count0 += 1
        N = int(string[count0:2*count0+1], 2) - 1
        return 2**N + int(string[2*count0+1:2*count0+1+N+1], 2)
    
    def _gamma_encode(self, n):
        r"""
        Encode the given positive integer with the Elias gamma code.
        
        INPUT:

        - ``n`` -- a positive integer.
          
        OUTPUT:

        - The Elias gamma code of ``n``.
        """
        N = int(log(n, 2))
        return N*'0' + bin(n)[2:]

    def _gamma_decode(self, string):
        r"""
        Decode the given Elias gamma code.
        
        INPUT:

        - ``string`` -- an Elias gamma code.
          
        OUTPUT:

        - The Elias gamma decoding of ``string``.
        """
        count0 = 0
        while string[count0] == '0':
            count0 += 1
        return int(string[count0:2*count0+1], 2)
    
    def _omega_encode(self, n):
        r"""
        Encode the given positive integer with the Elias omega code.
        
        INPUT:

        - ``n`` -- a positive integer.
          
        OUTPUT:

        - The Elias omega code of ``n``.
        """
        output = '0'
        while n != 1:
            bin_n = bin(n)[2:]
            output = bin_n + output
            n = len(bin_n) - 1
        return output
    
    def _omega_decode(self, string):
        r"""
        Decode the given Elias omega code.
        
        INPUT:

        - ``string`` -- an Elias omega code.
          
        OUTPUT:

        - The Elias omega decoding of ``string``.
        """
        n = 1
        i = 0
        while string[i] != '0':
            tmp = int(string[i:i+n+1], 2)
            i += n + 1
            n = tmp
        return n
