r"""
Lempel Ziv 77 Coding

This module implements Lempel-Ziv-77. 

AUTHORS:

- Jan Wabbersen (2015-04-27): initial version.
"""

#*****************************************************************************
#       Copyright (C) 2015 Jan Wabbersen <jan.wabbersen@googlemail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from sage.structure.sage_object import SageObject
from misc import SimpleTable

class LZ77(SageObject):
    r"""
    This class implements Lempel-Ziv-77.
    
    It can build a LZ77 encoding in the form of triples from a given
    string. 
    
    INPUT:

        - ``search_buffer_size`` -- (default: 96) the size of the used
          search buffer.
          
        - ``lookahead_buffer_size`` -- (default: 32) the size of the
          look-ahead buffer.
          
        - ``verbose`` -- (default: False) if True, print intermediate
          data of building the code.
       
    EXAMPLES::
    
        sage: from sage.coding.source_coding.lz77 import LZ77
        sage: lz = LZ77()
        sage: triples = lz.encode("Encode me, please!"); triples
        [(0, 0, 'E'),
        (0, 0, 'n'),
        (0, 0, 'c'),
        (0, 0, 'o'),
        (0, 0, 'd'),
        (0, 0, 'e'),
        (0, 0, ' '),
        (0, 0, 'm'),
        (3, 1, ','),
        (4, 1, 'p'),
        (0, 0, 'l'),
        (8, 1, 'a'),
        (0, 0, 's'),
        (11, 1, '!')]
        sage: lz.decode(triples)
        'Encode me, please!'

    This example shows how to use the class. If you want to define the
    the size of the used search buffer or look-ahead buffer, you can do
    that with ``search_buffer_size`` and ``lookahead_buffer_size``::
    
        sage: lz2 = LZ77(search_buffer_size=5, lookahead_buffer_size=4, verbose=True)
        sage: lz2.encode("This is just an example!")
        triple: (offset, length, next symbol)
        | search window | look-ahead buffer | triple      |
        |               | This              | (0, 0, 'T') |
        | T             | his               | (0, 0, 'h') |
        | Th            | is i              | (0, 0, 'i') |
        | Thi           | s is              | (0, 0, 's') |
        | This          |  is               | (0, 0, ' ') |
        | This          | is j              | (3, 3, 'j') |
        |  is j         | ust               | (0, 0, 'u') |
        | is ju         | st a              | (4, 1, 't') |
        |  just         |  an               | (5, 1, 'a') |
        | ust a         | n ex              | (0, 0, 'n') |
        | st an         |  exa              | (3, 1, 'e') |
        |  an e         | xamp              | (0, 0, 'x') |
        | an ex         | ampl              | (5, 1, 'm') |
        |  exam         | ple!              | (0, 0, 'p') |
        | examp         | le!               | (0, 0, 'l') |
        | xampl         | e!                | (0, 0, 'e') |
        | ample         | !                 | (0, 0, '!') |
        [(0, 0, 'T'),
         (0, 0, 'h'),
         (0, 0, 'i'),
         (0, 0, 's'),
         (0, 0, ' '),
         (3, 3, 'j'),
         (0, 0, 'u'),
         (4, 1, 't'),
         (5, 1, 'a'),
         (0, 0, 'n'),
         (3, 1, 'e'),
         (0, 0, 'x'),
         (5, 1, 'm'),
         (0, 0, 'p'),
         (0, 0, 'l'),
         (0, 0, 'e'),
         (0, 0, '!')]
    """
    def __init__(self, search_buffer_size=96, lookahead_buffer_size=32,
                 verbose=False):
        r"""
        Constructor for LZ77.

        See the docstring of this class for full documentation.

        EXAMPLES::

            sage: from sage.coding.source_coding.lz77 import LZ77
            sage: lz2 = LZ77(search_buffer_size=5, lookahead_buffer_size=4)
        """
        self._sb_size = search_buffer_size
        self._lab_size = lookahead_buffer_size
        self._verbose = verbose
        
    def _find_longest_prefix(self, window, sep):
        r"""
        Find the longest prefix of window[sep:] in window, starting in
        window[:sep]. 

        INPUT:

        - ``window`` -- the window.
        
        - ``sep`` -- index that separates the window.
        
        OUTPUT:
        
        - A list consisting of the offset between ``sep`` and
          the longest prefix found and the length of it.
          
        TESTS::
        
            sage: from sage.coding.source_coding.lz77 import LZ77
            sage: lz2 = LZ77()
            sage: lz2._find_longest_prefix("this is the window", 8)
            [8, 2]
        """
        search_buffer = window[:sep]
        lookahead_buffer = window[sep:]
        max_len = 0
        max_index = sep
        for i in xrange(len(search_buffer)):
            j = 0
            try:
                while window[i+j] == lookahead_buffer[j]:
                    j += 1
            # If the longest prefix is the complete look-ahead buffer.
            except IndexError:
                pass
            if j > max_len:
                max_len = j
                max_index = i
        return [sep - max_index, max_len]
                
    def encode(self, string):
        r"""
        Encode the given string based on the current window.
        
        INPUT:

        - ``string`` -- a string of symbols over an alphabet.
          
        OUTPUT:

        - A Lempel-Ziv-77 encoding of ``string``.
        
        EXAMPLES:
        
        This example illustrates how to encode a string::
        
            sage: from sage.coding.source_coding.lz77 import LZ77
            sage: lz = LZ77(search_buffer_size=8, lookahead_buffer_size=8)
            sage: lz.encode("Encode me, please!")
            [(0, 0, 'E'),
             (0, 0, 'n'),
             (0, 0, 'c'),
             (0, 0, 'o'),
             (0, 0, 'd'),
             (0, 0, 'e'),
             (0, 0, ' '),
             (0, 0, 'm'),
             (3, 1, ','),
             (4, 1, 'p'),
             (0, 0, 'l'),
             (8, 1, 'a'),
             (0, 0, 's'),
             (8, 1, '!')]
        """
        if self._verbose:
            t = SimpleTable(["search window", "look-ahead buffer", "triple"])
        encoded = []
        pos = 0
        while pos < len(string):
            window_start = max(pos - self._sb_size, 0)
            window = string[window_start:pos+self._lab_size]
            [offset, length] = self._find_longest_prefix(
                window, pos - window_start)
            try:
                symbol = string[pos+length]
            # If the last triple does not encode a new character.
            except IndexError:
                symbol = ""
            encoded.append((offset, length, symbol))
            if self._verbose:
                sb = string[window_start:pos]
                la = string[pos:pos+self._lab_size]
                t.add_row([sb, la, (offset, length, symbol)])
            pos += length + 1
        if self._verbose:
            print "triple: (offset, length, next symbol)"
            t.print_me()
        return encoded
    
    def decode(self, triples):
        r"""
        Decode the given LZ77 tripels.
        
        INPUT:

        - ``triples`` -- a list of LZ77 triples.
          
        OUTPUT:

        - The LZ77 decoding of ``triples``.
        
        EXAMPLES:
        
        This example illustrates how to encode and then decode a
        string::
        
            sage: from sage.coding.source_coding.lz77 import LZ77
            sage: lz = LZ77()
            sage: triples = lz.encode("Encode me, please!"); triples
            [(0, 0, 'E'),
             (0, 0, 'n'),
             (0, 0, 'c'),
             (0, 0, 'o'),
             (0, 0, 'd'),
             (0, 0, 'e'),
             (0, 0, ' '),
             (0, 0, 'm'),
             (3, 1, ','),
             (4, 1, 'p'),
             (0, 0, 'l'),
             (8, 1, 'a'),
             (0, 0, 's'),
             (11, 1, '!')]
            sage: lz.decode(triples)
            'Encode me, please!'
        """
        decoded = []
        for offset, length, next_char in triples:
            if offset == 0:
                decoded.append(next_char)
            else:
                pos = len(decoded)
                for i in xrange(length):
                    decoded.append(decoded[pos - offset + i])
                decoded.append(next_char)
        return ''.join(decoded)
