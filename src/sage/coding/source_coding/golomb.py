r"""
Golomb Coding

This module implements functionalities relating to Golomb coding.

AUTHORS:

- Jan Wabbersen (2015-04-27): initial version.
"""

#*****************************************************************************
#       Copyright (C) 2015 Jan Wabbersen <jan.wabbersen@googlemail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from math import ceil, floor, log

from sage.structure.sage_object import SageObject

class Golomb(SageObject):
    r"""
    This class implements Golomb coding for encoding run-lengths.
    
    It can encode runs of 0's in binary streams. It can also just build
    a Golomb code for non-negative integers.
    
    INPUT:

        - ``m`` -- the divisor parameter for building a Golomb code.
       
    EXAMPLES::
    
        sage: from sage.coding.source_coding.golomb import Golomb
        sage: m = 10
        sage: g = Golomb(m)
        sage: encoded = g.encode("000000010000000000100000110000000000001000"); encoded
        '0011011000001010000100100011'
        sage: g.decode(encoded)
        '000000010000000000100000110000000000001000'

    This example shows how to use the class, if you want to use Golomb
    for encoding run-lengths. If you just want to calculate the Golomb
    code for a non-negative integer, you can do this way::
    
        sage: m2 = 12
        sage: g2 = Golomb(m2)
        sage: g2.golomb(240)
        '111111111111111111110000'
    """
    
    def __init__(self, m):
        r"""
        Constructor for Golomb.

        See the docstring of this class for full documentation.

        EXAMPLES::

            sage: from sage.coding.source_coding.golomb import Golomb
            sage: m = 8
            sage: g = Golomb(m)
        """
        # Divisor
        self._m = m
        self._b = int(ceil(log(m, 2)))
        self._threshold = 2**self._b - m

    def golomb(self, n):
        r"""
        Calculate the Golomb code for the passed non-negative integer
        with the current parameter ``m``.

        INPUT:

        - ``n`` -- to be encoded non-negative integer.
        
        OUTPUT:

        - The Golomb code for ``string``.
          
        EXAMPLES::
        
            sage: from sage.coding.source_coding.golomb import Golomb
            sage: m = 10
            sage: g = Golomb(m)
            sage: g.golomb(23)
            '110011'
        """
        if n < 0:
            raise ValueError("Input must be an integer greater or equal to 0.")
        q = int(floor(n * 1.0 / self._m))
        r = n - q*self._m
        if r < self._threshold:
            return q*'1' + '0' + format(r, '0' + str(self._b - 1) + 'b')
        else:
            if self._b < 1: return q*'1' + '0'
            return q*'1' + '0' + format(r + self._threshold,
                                        '0' + str(self._b) + 'b')
                
    def encode(self, string):
        r"""
        Encode the given binary string (with runs of 0's).
        
        INPUT:

        - ``string`` -- a binary string.
          
        OUTPUT:

        - A Golomb encoding of ``string``.
        
        EXAMPLES:
        
        This example illustrates how to encode a binary string::
        
            sage: from sage.coding.source_coding.golomb import Golomb
            sage: m = 10
            sage: g = Golomb(m)
            sage: g.encode("00000000000001000000101")
            '110011011000001'
        """
        count0 = 0
        output = ''
        for s in string:
            if s == '0':
                count0 += 1
            elif s == '1':
                output += self.golomb(count0)
                count0 = 0
            else:
                raise ValueError("Input must be a binary string.")
        # Save if input stream ends with 0 or 1 for decoding purposes:
        # 0 for ending with 0, 1 for ending with 1.
        if count0 == 0:
            output = '1' + output
        else:
            output += self.golomb(count0)
            output = '0' + output
        return output
        
    def decode(self, string):
        r"""
        Decode the given Golomb encoding based on the current ``m``.
        
        INPUT:

        - ``string`` -- a binary string.
          
        OUTPUT:

        - The Golomb decoding of ``string``.
        
        EXAMPLES:
        
        This example illustrates how to encode and then decode a
        string::
        
            sage: from sage.coding.source_coding.golomb import Golomb
            sage: m = 10
            sage: g = Golomb(m)
            sage: encoded = g.encode("01000000000000100000010000"); encoded
            '0000110010011000100'
            sage: g.decode(encoded)
            '01000000000000100000010000'
        """
        b = self._b
        count1 = 0
        output = ''
        # Starting index. First bit is used for encoding purposes.
        i = 1
        while i < len(string):
            if string[i] == '1':
                count1 += 1
                i += 1
            else:
                r = int(string[i+1:i+b], 2)
                if r < self._threshold:
                    output += (self._m*count1 + r) * '0' + '1'
                    i += b
                else:
                    r = int(string[i+1:i+b+1], 2)
                    output += (self._m*count1 + r - self._threshold)*'0' + '1'
                    i += b + 1
                count1 = 0
        # Delete last bit if it was appended only for encoding purposes.
        if string[0] == '0':
            output = output[:-1]
        return output
