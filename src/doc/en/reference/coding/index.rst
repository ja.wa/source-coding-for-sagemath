.. _sage.coding:

Coding Theory
=============

.. toctree::
   :maxdepth: 1

   sage/coding/codes_catalog
   sage/coding/linear_code
   sage/coding/code_constructions
   sage/coding/guava
   sage/coding/sd_codes
   sage/coding/code_bounds
   sage/coding/codecan/autgroup_can_label
   sage/coding/delsarte_bounds
   sage/coding/source_coding/prefix_coding
   sage/coding/source_coding/misc
   sage/coding/source_coding/shannon
   sage/coding/source_coding/shannon_fano
   sage/coding/source_coding/shannon_fano_elias
   sage/coding/source_coding/huffman
   sage/coding/source_coding/arithmetic_coding
   sage/coding/source_coding/golomb
   sage/coding/source_coding/elias_codes
   sage/coding/source_coding/lz77

.. include:: ../footer.txt
